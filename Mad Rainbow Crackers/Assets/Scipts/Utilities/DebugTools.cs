﻿using System.Globalization;
using JetBrains.Annotations;
using UnityEngine;

namespace Assets.Scripts.Utilities
{
	[UsedImplicitly]
	public class DebugTools : MonoBehaviour
	{
		// Tool for debugging an elapsed time between two call
		private static double _elapsedTime = -1;
		public static void ResetTime()
		{
			_elapsedTime = 0;
		}
		public static void PrintTime(string label)
		{
			Debug.Log(label + " : " + _elapsedTime);
		}

		private void FixedUpdate()
		{
			_elapsedTime = (_elapsedTime >= 0 ? _elapsedTime + Time.deltaTime : -1);
		}
	}
}